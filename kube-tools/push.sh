#!/bin/bash

set -o allexport; source .env; set +o allexport
echo "🐋 ${IMAGE_NAME}:${IMAGE_TAG}"

docker login registry.gitlab.com -u ${GITLAB_HANDLE} -p ${GITLAB_TOKEN_ADMIN}

#docker tag multi-tools registry.gitlab.com/wasmcooking/images/${IMAGE_NAME}:latest
docker tag ${IMAGE_NAME} registry.gitlab.com/${REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}
docker push registry.gitlab.com/${REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}
